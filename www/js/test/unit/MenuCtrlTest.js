'use strict';
describe('DidIt controllers', function () {
    beforeEach(module('diditApp'));
    
    describe('MenuCtrl', function () {
        it('should create "sections" model with 3 section',
            inject(function ($controller) {
                var scope = {},
                    ctrl = $controller('MenuCtrl', {$scope: scope});
                expect(scope.sections.length).toBe(3);
            }));
        it('should create a link to the home section for the first section',
            inject(function ($controller) {
                var scope = {},
                    ctrl = $controller('MenuCtrl', {$scope: scope});
                expect(scope.sections[0].url).toBe("http://didit.juanwolf.fr/index.html");
            }));
        it('should create a link to the setting section for the second section',
            inject(function ($controller) {
                var scope = {},
                    ctrl = $controller('MenuCtrl', {$scope: scope});
                expect(scope.sections[1].url).toBe("http://didit.juanwolf.fr/settings.html");
            }));
        it('should create a link to the "about" section for the last section',
            inject(function ($controller) {
                var scope = {},
                    ctrl = $controller('MenuCtrl', {$scope: scope});
                expect(scope.sections[2].url).toBe("http://didit.juanwolf.fr/about.html");
            }));
    });
});