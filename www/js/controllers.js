var diditApp = angular.module('diditApp', []);

diditApp.controller('MenuCtrl', function($scope) {
    $scope.sections = [
        {'name': 'Home',
         'url': 'http://didit.juanwolf.fr/index.html'},
        {'name': 'Settings',
            'url': 'http://didit.juanwolf.fr/settings.html'},
         {'name': 'About',
         'url': 'http://didit.juanwolf.fr/about.html'}
    ];
});

function taskController($scope) {    
    $scope.tasks = [{
        name: "A simple example here",
        done: false,
        date: new Date(),
        note: null
    }];
    
    $scope.addTask = function() {
        var task = {
            name: $scope.taskName,
            done: false,
            date: new Date(),
            note: null
        };  
        $scope.tasks.push(task);  
    }
}

diditApp.controller('TaskListCtrl', taskController);